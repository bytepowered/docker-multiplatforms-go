#!/bin/bash

DATE_VERSION=`date +%F`
IMAGE_REPO=bytepowered-docker.pkg.coding.net/automator/images
IMAGE_NAME=multi-platforms-go

IMAGE_VERSION=${IMAGE_REPO}/${IMAGE_NAME}:${DATE_VERSION}

# 跨平台构建，需要启用 Docker BuildKit:
# https://docs.docker.com/buildx/working-with-buildx/
echo "[IMAGE BUILD] Using docker build kit ..."
docker buildx create --use --name gobuild --driver docker-container --driver-opt image=dockerpracticesig/buildkit:master
docker buildx use gobuild

# 需要多平台支持: --platform linux/arm,linux/arm64,linux/amd64
docker buildx build \
    --platform linux/amd64,linux/arm64 \
    --push \
    -t ${IMAGE_VERSION} .

FROM --platform=$BUILDPLATFORM golang:alpine AS build
ARG BUILDPLATFORM
ARG TARGETPLATFORM
ENV GOPROXY=https://goproxy.cn,direct
RUN sed -i 's/dl-cdn.alpinelinux.org/mirrors.ustc.edu.cn/g' /etc/apk/repositories \
    && go get github.com/mitchellh/gox && apk add --no-cache upx
COPY . /src/
WORKDIR /src
RUN echo "Build: $BUILDPLATFORM, Target: $TARGETPLATFORM" \
    && gox -osarch=$TARGETPLATFORM -ldflags="-w -s" -output="multiplatforms" \
    && upx -9 -q multiplatforms

FROM --platform=$TARGETPLATFORM alpine:3.14
WORKDIR /app
COPY --from=build /src/multiplatforms /app/
CMD ["./multiplatforms"]

package main

import (
	"fmt"
	"os/exec"
)

func main() {
	c := exec.Command("uname", "-a")
	if o, err := c.Output(); err != nil {
		fmt.Printf("Error: %s", err.Error())
	} else {
		fmt.Printf("System: %s", string(o))
	}
}
